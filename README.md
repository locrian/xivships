# xivships

Bash tool to generate a random FFXIV ship.

---

## How to use

Download `xivchars.txt` and `xivships.sh` into your home directory.

In your terminal, run:
`bash xivships.sh`

---

## Notes

You need to have write permissions for wherever you save these files, since the script creates (and then destroys) a temporary file called `shuffle`.

`xivships.sh` contains the line `file=xivchars.txt`. If you save these files anywhere else besides your home directory, you’ll need to update this line to reflect the new location. (For example, `file=~/ficprompts/xivchars.txt` if you’ve moved `xivchars.txt` into a folder called `ficprompts`.)

I was losing my mind keeping track of everyone in this list, so it’s in alphabetical order now. This does mean that unfortunately the underage characters are no longer as easy to remove if you’re so inclined. No prepubescent characters are included.

This took me like 45 minutes, including time spent tearing my hair out over a typo I failed to notice. Take it, modify it, replace the character list with your own favorite blorbos, whatever.
