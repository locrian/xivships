cp ~/directory/where/code-in-progress/lives/xivchars.txt ~/path/to/local/copy/of/gemini/content/xivchars.txt
cp ~/directory/where/code-in-progress/lives/xivchars.txt ~/path/to/local/copy/of/web/content/xivchars.txt
scp ~/directory/where/code-in-progress/lives/xivchars.txt user@remote-server:/path/to/gemini/content
scp ~/directory/where/code-in-progress/lives/xivchars.txt user@remote-server:/path/to/web/content
cp ~/directory/where/code-in-progress/lives/xivchars.txt ~/git/xivships/xivchars.txt
cd ~/git/xivships
git commit -am 'Characters added'
git push
tput bel
